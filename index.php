<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    
    $sheep = new Animal ("Shaun");
    echo "Nama Hewan : ". $sheep->name ."<br>";
    echo "Jumlah Kaki : ". $sheep->legs ."<br>";
    echo "Cold Blooded : ". $sheep->cold_blooded ."<br><br>";

    $kodok = new Frog ('Buduk');
    echo "Nama Hewan : ". $kodok->name ."<br>";
    echo "Jumlah Kaki : ". $kodok->legs ."<br>";
    echo "Cold Blooded : ". $kodok->cold_blooded ."<br>";
    echo "Jump : ". $kodok->Lompat() . "<br><br>";

    $sungokong = new Ape ("Kera Sakti");
    echo "Nama Hewan : ". $sungokong->name ."<br>";
    echo "Jumlah Kaki : ". $sungokong->legs ."<br>";
    echo "Cold Blooded : ". $sungokong->cold_blooded ."<br>";
    echo "Yell : ". $sungokong->Teriak() . "<br><br>";


    


?>